import React, { Component } from 'react';
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import Content from './Components/DashboardContent';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <WithHeaderFooter
                {...this.state}
                bodyContent={<Content />}
            />
        )
    }
}

export default Dashboard;