import React from 'react';
import PropTypes from 'prop-types';
import style from './Style'

import { Panel, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

const LoginForm = props => (
    <div style={style.divStyle}>
        <Panel style={style.panelStyle}>
            <ControlLabel className="error">{props.msg}</ControlLabel>
            <form onSubmit={props.onSubmit} className="LoginForm" id="loginForm">
                <FormGroup controlId="formEmail">
                    <FormControl
                        type="email"
                        placeholder="Email Address"
                        name="email"
                        value={props.formData.email}
                        onChange={props.onChange}
                    />
                    {props.errors.email && <ControlLabel className="error">{props.errors.email}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formPassword">
                    <FormControl
                        type="password"
                        name="password"
                        value={props.formData.password}
                        placeholder="Password"
                        onChange={props.onChange}
                    />
                    {props.errors.password && <ControlLabel className="error">{props.errors.password}</ControlLabel>}
                </FormGroup>
                <FormGroup style={style.buttonStyle} controlId="formSubmit">
                    <Button bsStyle="primary" type="submit"> Login</Button>
                    <div className="hints">
                        email: admin@mail.com <br />
                        password: password
                    </div>
                </FormGroup>
            </form>
        </Panel>
    </div>
);

LoginForm.propTypes = {
    formData: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    msg: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
}
export default LoginForm;