import React, { Component } from 'react';
import LoginForm from './Components/LoginForm';
import './Components/Login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            credintial: {
                email: 'admin@mail.com',
                password: 'password'
            },
            formData: {
                email: '',
                password: ''
            },
            errors: {
                email: '',
                password: ''
            },
            msg: ''
        };
        this.onChange = this.onChange.bind(this);
        this.basicValidation = this.basicValidation.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(event) {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }
    basicValidation(data) {
        const errors = {};
        const errorMsgs = {
            email: 'Email cannot be blank.',
            password: 'Password caanot be blank.'
        };
        Object.entries(data).forEach(([dataKey, dataValue]) => {
            if (Object.keys(errorMsgs).includes(dataKey) && !dataValue.replace(/^\s+|\s+$/g, '')) {
                errors[dataKey] = errorMsgs[dataKey];
            }
        });
        return errors;
    }
    onSubmit(event) {
        event.preventDefault();
        const { formData, credintial } = this.state;
        const errors = this.basicValidation(formData);
        if (Object.keys(errors).length > 0) {
            this.setState({ errors });
        } else {
            if (formData.email === credintial.email && formData.password === credintial.password) {
                console.log('Logging successfully');
                this.setState({
                    msg: "Logging successfully"
                });
                this.props.history.push('/user/list');
            } else {
                console.log('invalid password');
                this.setState({
                    errors: {
                        email: '',
                        password: ''
                    },
                    msg: "Invalid credintial"
                });
            }
        }
    }
    render() {
        return (
            <div align="center" className="Login">
                <LoginForm
                    {...this.state}
                    onChange={this.onChange}
                    onSubmit={this.onSubmit}
                />
            </div>
        )
    }
}

export default Login;