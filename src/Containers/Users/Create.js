import React, { Component } from 'react';
import { connect } from 'react-redux';
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import UserForm from './Components/UserForm';
import { onAdd } from '../../Store/Actions/Users';
import './Components/User.css';

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            msg: '',
            formData: {
                fullName: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: ''
            },
            errors: {
                fullName: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: ''
            }
        }
        this.onChange = this.onChange.bind(this);
        this.basicValidation = this.basicValidation.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(event) {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }
    basicValidation(data) {
        const errors = {};
        const errorMsgs = {
            fullName: 'Full Name cannot be blank.',
            email: 'Email cannot be blank.',
            password: 'Password cannot be blank.',
            dob: 'DOB cannot be blank.',
            designation: 'Please select designation',
            status: 'Please select status'
        };
        Object.entries(data).forEach(([dataKey, dataValue]) => {
            if (Object.keys(errorMsgs).includes(dataKey) && !dataValue.replace(/^\s+|\s+$/g, '')) {
                errors[dataKey] = errorMsgs[dataKey];
            }
        });
        return errors;
    }
    // call on Delete event.
    onSubmit(event) {
        event.preventDefault();
        const { formData } = this.state;
        const errors = this.basicValidation(formData);
        if (Object.keys(errors).length > 0) {
            this.setState({ errors });
        } else {
            this.props.onAdd({ ...formData, id: this.props.data.users.length + 1}); // Call from Actions
            this.props.history.push('/user-list');
        }
    }

    render() {
        return (
            <WithHeaderFooter
                {...this.props}
                title="Create New User"
                bodyContent={
                    <table className="user-container">
                        <tbody>
                            <tr>
                                <td>
                                    <UserForm
                                        {...this.state}
                                        designation={this.props.data.designation}
                                        onChange={this.onChange}
                                        onSubmit={this.onSubmit}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                }
            />
        )
    }
}
const mapStateToProps = state => {
    return {
        data: state.users
    }
}
const mapDispatchToProps = {
    onAdd
}
const Container = connect(mapStateToProps, mapDispatchToProps)(Create);
export default Container;