import React from 'react';
import PropsType from 'prop-types';
import { Link } from 'react-router-dom';

const ListItem = ({ item, onDelete }) => (
    <tr key={item.id}>
        <td>{item.id}</td>
        <td>{item.fullName}</td>
        <td>{item.email}</td>
        <td>{item.password}</td>
        <td>{item.dob}</td>
        <td>{item.designation}</td>
        <td>{item.status}</td>
        <td>
            <Link to={`/user/update/${item.id}`}>Update</Link> |
            <Link to="#" onClick={event => onDelete(event, item.id)}>Delete</Link>
        </td>
    </tr>
);

// Props Validation
ListItem.propsType = {
    item: PropsType.object.isRequired,
    onDelete: PropsType.func.isRequired
}
export default ListItem;