import React from 'react';
import PropTypes from 'prop-types';
import style from './Style'

import { Panel, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';

const UserForm = props => (
    <div style={style.divStyle}>
        <div style={style.error}>{props.msg}</div>
        <Panel style={style.panelStyle}>
            <form onSubmit={props.onSubmit}>
                <FormGroup controlId="formFullName">
                    <ControlLabel>Full Name</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder="Full Name"
                        name="fullName"
                        value={props.formData.fullName}
                        onChange={props.onChange}
                    />
                    {props.errors.fullName && <ControlLabel className="error">{props.errors.fullName}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formEmail">
                    <ControlLabel>Email</ControlLabel>
                    <FormControl
                        type="email"
                        name="email"
                        value={props.formData.email}
                        placeholder="Email"
                        onChange={props.onChange}
                    />
                    {props.errors.email && <ControlLabel className="error">{props.errors.email}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formPassword">
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        type="password"
                        name="password"
                        value={props.formData.password}
                        placeholder="Password"
                        onChange={props.onChange}
                    />
                    {props.errors.password && <ControlLabel className="error">{props.errors.password}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formDOB">
                    <ControlLabel>DOB</ControlLabel>
                    <FormControl
                        type="date"
                        name="dob"
                        value={props.formData.dob}
                        placeholder="DD/MM/YYYY"
                        onChange={props.onChange}
                    />
                    {props.errors.dob && <ControlLabel className="error">{props.errors.dob}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formControlsSelectDesignation">
                    <ControlLabel>Select Designation</ControlLabel>
                    <FormControl
                        componentClass="select"
                        name="designation"
                        value={props.formData.designation}
                        placeholder="Select Designation"
                        onChange={props.onChange}
                    >
                        {props.designation.map((item, idx) => <option key={idx} value={item.value}>{item.label}</option>)}
                    </FormControl>
                    {props.errors.designation && <ControlLabel className="error">{props.errors.designation}</ControlLabel>}
                </FormGroup>
                <FormGroup controlId="formControlsSelectStatus">
                    <ControlLabel>Select Status</ControlLabel>
                    <FormControl
                        componentClass="select"
                        placeholder="Select Status"
                        name="status"
                        value={props.formData.status}
                        onChange={props.onChange}
                    >
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </FormControl>
                    {props.errors.status && <ControlLabel className="error">{props.errors.status}</ControlLabel>}
                </FormGroup>
                <FormGroup style={style.buttonStyle} controlId="formSubmit">
                    <Button bsStyle="primary" type="submit"> Create</Button>
                </FormGroup>
            </form>
        </Panel>
    </div>
);

UserForm.propTypes = {
    formData: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    designation: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}
export default UserForm;