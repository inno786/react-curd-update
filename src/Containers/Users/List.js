import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import ListItem from './Components/ListItem';
import { onDelete } from '../../Store/Actions/Users';
import './Components/User.css';

class List extends Component {
    constructor(props) {
        super(props);
        this.onDelete = this.onDelete.bind(this);
    }
    // call on Delete event.
    onDelete(event, id) {
        event.preventDefault();
        this.props.onDelete(id); // Call from Actions
    }

    render() {
        const { users } = this.props.data;
        return (
            <WithHeaderFooter
                {...this.props}
                title="User List"
                bodyContent={
                    <table border={1} className="user-container">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>DOB</th>
                                <th>Designation</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.length && users.map((row, idx) => (
                                <ListItem
                                    item={row}
                                    onDelete={this.onDelete}
                                    key={idx}
                                />
                            ))}
                            <tr>
                                <td colSpan="8">
                                    <Link to="/user/create">Add New User</Link>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                }
            />
        )
    }
}
const mapStateToProps = state => {
    return {
        data: state.users
    }
}
const mapDispatchToProps = {
    onDelete
}
const Container = connect(mapStateToProps, mapDispatchToProps)(List);
export default Container;