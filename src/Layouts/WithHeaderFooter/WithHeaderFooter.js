import React from 'react';
import PropTypes from 'prop-types';
import Wrapper from '../../Components/Wrapper/Wrapper';
import Sidebar from '../../Components/Sidebar/Sidebar';
import styles from './WithHeaderFooterStyle';

const WithHeaderFooter = props => (
    <Wrapper id={2}>
        <table style={styles.table} border={1} align="center">
            <tbody>
                <tr style={styles.header}>
                    <td colSpan={2}>Header</td>
                </tr>
                <tr>
                    <td style={styles.sidebar}>
                        <Sidebar />
                    </td>
                    <td style={styles.center}>
                        <h4 style={styles.title}>{props.title}</h4>
                        {props.bodyContent}
                    </td>
                </tr>
                <tr style={styles.footer}>
                    <td colSpan={2}>Footer</td>
                </tr>
            </tbody>
        </table>
    </Wrapper>
)
WithHeaderFooter.defaultProps = {
    title: ''
};

WithHeaderFooter.propTypes = {
    title: PropTypes.string.isRequired,
    bodyContent: PropTypes.any
}
export default WithHeaderFooter;