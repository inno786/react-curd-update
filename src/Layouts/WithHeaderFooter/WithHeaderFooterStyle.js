
const styles = {
    table: {
        width: '80%'
    },
    header: {
        width: '100%',
        padding: 20,
        textAlign: 'center'
    },
    sidebar: {
        width: '20%',
        padding: 20,
        textAlign: 'center'
    },
    center: {
        width: '80%',
        padding: 20,
    },
    footer: {
        width: '100%',
        padding: 20,
        textAlign: 'center'
    },
    title: {
        textAlign: 'left'
    }
}

export default styles;