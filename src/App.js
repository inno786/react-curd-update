import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { Provider } from 'react-redux'; // HOC provided by Redux
import Home from './Containers/Home/Home';
import Counter from './Containers/LifeCycle/Counter';
import Login from './Containers/Login/Login';
import Dashboard from './Containers/Dashboard/Dashboard';
import UserList from './Containers/Users/List';
import UserCreate from './Containers/Users/Create';
import UserUpdate from './Containers/Users/Update';
import store from './Store';  // Store that is connected from redux

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/dashboard' component={Dashboard} />
            <Route exact path='/user/list' component={UserList} />
            <Route exact path='/user/create' component={UserCreate} />
            <Route exact path='/user/update/:id' component={UserUpdate} />
            <Route exact path='/life-cycle' component={Counter} />
            <Route path="*" component={() =>(<h4>Page Not Found</h4>)} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
