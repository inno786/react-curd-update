import { USER_LIST, ON_DELETE, ON_ADD, ON_UPDATE } from '../Constants/ActionTypes';

const initilize = {
    users: [
        { id: 1, fullName: 'Sunil Saurav', email: 'sunil.saurav@gmail.com', password: 123456, dob: '01/01/1985', designation: 'CEO', status: 'Active' },
        { id: 2, fullName: 'Gobind Singh', email: 'gobind@gmail.com', password: 123456, dob: '01/01/1987', designation: 'ReactJs Developer', status: 'Active' },
        { id: 3, fullName: 'Rahul', email: 'rahul@gmail.com', password: 123456, dob: '01/01/1990', designation: 'Angular Developer', status: 'Inactive' },
    ],
    designation: [
        { id: 1, label: 'CEO', value: 'CEO' },
        { id: 2, label: 'ReactJs Developer', value: 'ReactJs Developer' },
        { id: 3, label: 'Angular Developer', value: 'Angular Developer' },
    ]
};

const users = (state = initilize, action) => {
    switch (action.type) {
        case USER_LIST:
            return {
                ...state
            }
        case ON_DELETE:
            console.log('on delete on reducers =>', action);
            return {
                ...state,
                users: state.users.filter(item => item.id !== action.payload.id)
            }
        case ON_ADD:
            console.log('on Add on reducers =>', action);
            return {
                ...state,
                users: [...state.users, action.payload.data]
            }
        case ON_UPDATE:
            console.log('on update on reducers =>', action);
            const idx = state.users.findIndex(x => action.payload.id === x.id);
            return {
                ...state,
                users: [
                    ...state.users.slice(0, idx),
                    { ...state.users[idx], ...action.payload }
                ]
            }
        default:
            return {
                ...state
            }
    }
};

export default users;