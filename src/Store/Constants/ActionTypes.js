// Define Action types 
export const USER_LIST = 'USER_LIST';
export const ON_DELETE = 'ON_DELETE';
export const ON_ADD = 'ON_ADD';
export const ON_UPDATE = 'ON_UPDATE';
